'use strict';

module.exports = [
  './gulp/tasks/clean.js',
  './gulp/tasks/sass.js',
  './gulp/tasks/stylelint.js',
  './gulp/tasks/serve.js',
  './gulp/tasks/pug.js',
  './gulp/tasks/watch.js',
  './gulp/tasks/js.foundation.js',
  './gulp/tasks/css.foundation.js',
  './gulp/tasks/scripts.js',
  // './gulp/tasks/scriptsLint.js',
  './gulp/tasks/images',
  './gulp/tasks/copy.fonts.js'
];