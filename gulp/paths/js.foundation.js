'use strict';

module.exports = [
  './node_modules/jquery/dist/jquery.min.js',
  './node_modules/dropzone/dist/min/dropzone.min.js',
'./node_modules/select2/dist/js/select2.min.js',
'./node_modules/inputmask/dist/jquery.inputmask.bundle.js',
'./node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
  './source/libs/combo-tree-master/comboTreePlugin.js',
];
