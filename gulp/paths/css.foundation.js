'use strict';

module.exports = [
  './node_modules/bootstrap/dist/css/bootstrap-reboot.min.css',
  './node_modules/bootstrap/dist/css/bootstrap.css',
  './source/libs/select2.min.css',
  './node_modules/hamburgers/dist/hamburgers.min.css',
  './node_modules/dropzone/dist/min/dropzone.min.css',
  './source/libs/combo-tree-master/style.css',
];
