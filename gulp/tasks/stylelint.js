

module.exports = function() {
  var processors = [
      $.stylelint({
          reporters: [
              {formatter: 'string', console: true}
          ]
      }),
      // messages({
      //     selector: 'body:before'
      // })
  ];
  $.gulp.task('stylelint', function() {
    return $.gulp.src(["./source/style/**/*"])
      .pipe($.plumber())
      .pipe($.postcss(processors, {syntax: $.syntax_scss}))// (*) Добавляем линтер сюда
  })
};
