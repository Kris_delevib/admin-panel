'use strict';

module.exports = function() {

  $.gulp.task('sass', function() {
    return $.gulp.src('./source/style/app.scss')
      .pipe($.sourcemaps.init())
      .pipe($.plumber())
      .pipe($.sass()).on('error', $.notify.onError({ title: 'Style' }))
      // .pipe($.uncss({
      //   html: ['build/*.html']
      // }))
      .pipe($.postcss([$.autoprefixer(), $.cssnano()]))
      .pipe($.cssUnit({
      	type     :    'px-to-rem',
      	rootSize :    16
      }))
      .pipe($.sourcemaps.write())
      .pipe($.gulp.dest($.config.root + '/css'))
      .pipe($.browserSync.stream());
  })
};

    // return $.gulp.src('./source/style/**/*.scss')