'use strict';

module.exports = function() {
  $.gulp.task('images', () => {
    return $.gulp.src('./source/img/**/*.*', { since: $.gulp.lastRun('images') })
    	.pipe($.imagemin([
    		$.imagemin.gifsicle({interlaced: true}),
    		$.imagemin.jpegtran({progressive: true}),
    		$.imagemin.optipng({optimizationLevel: 5})
    	]))
      .pipe($.gulp.dest($.config.root + '/img'));
  });
};
