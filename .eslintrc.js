module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "jquery": true,
        "node": true
    },
    // "parser": "babel-eslint",
    "extends": "eslint:recommended"
};