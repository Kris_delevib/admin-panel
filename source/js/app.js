(function () {
    $('.phone').inputmask("+7 (999) 999-99-99");

    let filter = document.querySelector('.filter');
    let filterContent = document.querySelector('.filter-content');

    if (filter) {
        filter.addEventListener('click', (e) => {
            e.preventDefault();
            filterContent.classList.toggle('filter-content--open')
        });
    }

    let menuIcon = document.querySelector('.hamburger');
    let menuContent = document.querySelector('.sidebar');
    let sidebarOverlay = document.querySelector('.sidebar-overlay');

    if (menuIcon) {
        menuIcon.addEventListener('click', (e) => {
            e.preventDefault();
            console.log(this)
            menuContent.classList.toggle("sidebar-open");
            sidebarOverlay.classList.toggle("sidebar-overlay-open");
            menuIcon.classList.toggle("is-active");
            menuIcon.classList.toggle("hamburger-open");
        })
    }

    $(".keywords").select2({
        placeholder: "Начните вводить",
        tags: true,
        tokenSeparators: [',', ' ']
    });


    $(".select-custom").select2({
        width: 'resolve',
        placeholder: "Выберите",
    });


    $("div#document-upload").dropzone({url: "/file/post"});


    let testCategory = [
        {
            id: 0,
            label: 'choice 1  '
        }, {
            id: 1,
            label: 'choice 2',
            children: [
                {
                    id: 10,
                    label: 'choice 2 1'
                }, {
                    id: 11,
                    label: 'choice 2 2'
                }, {
                    id: 12,
                    label: 'choice 2 3'
                }
            ]
        }, {
            id: 2,
            label: 'choice 3'
        }, {
            id: 5,
            label: 'choice 6',
            children: [
                {
                    id: 50,
                    label: 'choice 6 1'
                }
            ]
        }, {
            id: 6,
            label: 'choice 7'
        }
    ];

    $('#category').comboTree({
        source: testCategory,
        isMultiple: true,
        cascadeSelect: true
    });
})();