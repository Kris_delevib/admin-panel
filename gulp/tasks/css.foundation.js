'use strict';

module.exports = function() {
  $.gulp.task('css:foundation', function() {
    return $.gulp.src($.path.cssFoundation)
      .pipe($.concatCss('foundation.css'))
      .pipe($.cleanCss())
      .pipe($.gulp.dest($.config.root + '/css'))
  })
};
