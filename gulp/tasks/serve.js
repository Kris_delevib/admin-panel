'use strict';

module.exports = function() {
  $.gulp.task('serve', function() {
    $.browserSync.init({
      // open: false,
      online: true,
      server: $.config.root
    });

    // $.browserSync.watch([$.config.root + '/**/*.*', '!**/*.css'], $.browserSync.reload);
    $.browserSync.watch([$.config.root + '/**/*.*', '!**/*.css']).on('change', $.browserSync.reload);
  });
};
