'use strict';

module.exports = function() {
  $.gulp.task('scripts', function() {
    return $.gulp.src($.path.app)
      .pipe($.sourcemaps.init())
      .pipe($.concat('app.js'))
      .pipe($.sourcemaps.write())
      .pipe($.gulp.dest($.config.root + '/js'))
  });
};
